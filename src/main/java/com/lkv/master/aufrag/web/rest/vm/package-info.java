/**
 * View Models used by Spring MVC REST controllers.
 */
package com.lkv.master.aufrag.web.rest.vm;
