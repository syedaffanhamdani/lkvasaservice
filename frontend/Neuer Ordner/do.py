#!/usr/bin/python

import sys, os, csv, time, json, codecs
#from simplexml import dumps
from flask import Flask, make_response
from flask_restful import reqparse, abort, Api, Resource

#def output_xml(data, code, headers=None):
#    """Makes a Flask response with a XML encoded body"""
#    resp = make_response(dumps({'response' :data}), code)
#    resp.headers.extend(headers or {})
#    return resp

list=[]
ddd={}

def readcsv(tid):
  with codecs.open('./data/'+tid+'/Telematik_'+tid+'.csv', "r", encoding='utf-8', errors='ignore' ) as csvfile:
    reader = csv.reader(csvfile, delimiter=';', quotechar='"')
    header = next(reader)
    i=0
    dd=[]
    for row in reader:
       dd.append(dict(zip(header, row)))
#       dd[str(i)]=d
       i+=1
    ddd[tid]=dd
    
trailer={}
  
app = Flask(__name__)
api = Api(app)

#def abort_if_todo_doesnt_exist(todo_id):
#    if todo_id not in dd:
#        abort(404, message="Todo {} doesn't exist".format(todo_id))

tids=[]
for dirname, dirnames, filenames in os.walk('./data/'):
  for subdirname in dirnames:
        tids.append(os.path.join(subdirname))

for tid in tids:
  readcsv(tid)

class TrailerIDs(Resource):
    def get(self):
        return tids

class ShowMax(Resource):
    def get(self, td):
        return len(ddd[td])

class ShowMoves(Resource):
    def get(self, td):
        m={}
        xml=""
        id=len(ddd[td])-1
        old=0
        while id > 0:
          dist=ddd[td][id]["DISTANCE"]
          if dist != old:
            m[str(dist)]={"lat": ddd[td][id]["LATITUDE"], "lon": ddd[td][id]["LONGITUDE"]}
#            xml+='<trkpt lat="'+ddd[td][id]["LATITUDE"]+'" lon="'+ddd[td][id]["LONGITUDE"]+'"></trkpt>\n'
            old=dist
          id=id-1
#        xml={ "gpx": { "trk": { "name": "Truck 4711", "trkseg": { "trkpt": [ { "-lat": "50.878831", "-lon": "8.929012" }, { "-lat": "50.878764", "-lon": "8.930789" } ]}}}}
#        api.representations['application/xml'] = output_xml
        del m[""]
        return m

class ShowLine(Resource):
    def get(self, td, line):
        return ddd[td][int(line)-1]

class ShowItem(Resource):
    def get(self, td, line, item):
#        abort_if_todo_doesnt_exist(id)
        return ddd[td][int(line)-1][item]

api.add_resource(TrailerIDs, '/t/')
api.add_resource(ShowMax, '/t/<td>/')
api.add_resource(ShowLine, '/t/<td>/<line>/')
api.add_resource(ShowItem, '/t/<td>/<line>/<item>/')
api.add_resource(ShowMoves, '/movement/<td>/')

if __name__ == '__main__':
#    app.run(host='0.0.0.0', debug=True)
    app.run(debug=True)
