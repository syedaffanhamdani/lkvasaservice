#!/usr/bin/python
# tested with python version 2.7.12, os: ubuntu 16.04

import sys, os, csv, time, json, codecs
from flask import Flask, make_response
from flask_restful import reqparse, abort, Api, Resource

list=[]
ddd={}

# this script has to be started in its residing directory
# zip data from Schmitz Cargobull AG has to be extracted to ./data
# relative to working directory

def readcsv(tid):
# special characters still "destroy" the word
  with codecs.open('./data/'+tid+'/Telematik_'+tid+'.csv', "r", encoding='utf-8', errors='ignore' ) as csvfile:
    reader = csv.reader(csvfile, delimiter=';', quotechar='"')
    header = next(reader)
    dd=[]
    for row in reader:
       dd.append(dict(zip(header, row)))
    ddd[tid]=dd
    
app = Flask(__name__)
api = Api(app)

tids=[]
for dirname, dirnames, filenames in os.walk('./data/'):
  for subdirname in dirnames:
        tids.append(os.path.join(subdirname))

for tid in tids:
  readcsv(tid)

class TrailerIDs(Resource):
    def get(self):
        return tids

class ShowMax(Resource):
    def get(self, td):
        return len(ddd[td])

class ShowMoves(Resource):
    def get(self, td):
#        m={}
        xml=""
        id=len(ddd[td])-1
        old=0
        while id > 0:
          dist=ddd[td][id]["DISTANCE"]
          if dist != old:
            lat=str(ddd[td][id]["LATITUDE"])
            lon=str(ddd[td][id]["LONGITUDE"])
            if lat != "" and lon != "":
#              m[str(dist)]={"lat": lat, "lon": lon}
              xml+='<trkpt lat="' + lat + '" lon="' + lon + '"></trkpt>\n'
              old=dist
          id=id-1
#        del m[""]
        xml='<gpx>\n<trk>\n<name>Truck ' + td + '</name>\n<trkseg>\n' + xml + '</trkseg>\n</trk>\n</gpx>\n'
        r=make_response(xml)
        r.mimetype = 'application/xml'
        return r

class ShowLine(Resource):
    def get(self, td, line):
        return ddd[td][int(line)-1]

class ShowItem(Resource):
    def get(self, td, line, item):
        return ddd[td][int(line)-1][item]

# show truck IDs
api.add_resource(TrailerIDs, '/t/', '/movement/')

# show number of entries for truck id
api.add_resource(ShowMax, '/t/<td>/')

# show a single entry for a truck id (i.e. one csv line)
api.add_resource(ShowLine, '/t/<td>/<line>/')

# show a single item in that line (i.e. LATITIDE)
api.add_resource(ShowItem, '/t/<td>/<line>/<item>/')

# export xml data of gps positions of a single truck
# compatible with gpxview
api.add_resource(ShowMoves, '/movement/<td>/')

if __name__ == '__main__':
#   default is listen on localhost
    app.run(debug=True)
#    app.run(host='0.0.0.0', debug=True)
